scop:eveniment.

regula_ 1
daca{ 
     mod_lucru:in_echipa
     mediu:insotit
     atitudine:entuziasta
}
atunci:
    tip_personalitate:extrovertit  fc(90).

regula_ 2
daca{ 
     mod_lucru:individual
     mediu:singur
     atitudine:rezervata
}
atunci:
    tip_personalitate:introvertit  fc(90).

regula_ 3
daca{ 
     mod_lucru:in_echipa
     mediu:insotit
     atitudine:rezervata
}
atunci:
    tip_personalitate:extrovertit  fc(80).


regula_ 4
daca{ 
     mod_lucru:individual
     mediu:insotit
     atitudine:rezervata
}
atunci:
    tip_personalitate:introvertit  fc(80).


regula_ 5
daca{ 
     mod_lucru:in_echipa
     mediu:singur
     atitudine:rezervata
}
atunci:
    tip_personalitate:introvertit  fc(70).


regula_ 6
daca{ 
     mod_lucru:singur
     mediu:insotit
     atitudine:entuziast
}
atunci:
    tip_personalitate:extrovertit  fc(70).


regula_ 7
daca{ 
     mod_lucru:in_echipa
     mediu:insotit
     atitudine:rezervata
}
atunci:
    tip_personalitate:extrovertit  fc(70).


regula_ 8
daca{ 
    tip_personalitate:introvertit
    usor_de_impresionat
    
}
atunci:
    fire_sensibila  fc(90).

regula_ 9
daca{ 
    tip_personalitate:extrovertit
    \+ usor_de_impresionat 
}
atunci:
   \+ fire_sensibila  fc(90).

regula_ 10
daca{ 
    tip_personalitate:extrovertit
    usor_de_impresionat
    
}
atunci:
    fire_sensibila  fc(70).

regula_ 11
daca{ 
    tip_personalitate:introvertit
    \+ usor_de_impresionat
    
}
atunci:
    \+ fire_sensibila  fc(70).

regula_ 12
daca{ 
    bun_critic
    atent_la_detalii
    frica_esec 
}
atunci:
    perfectionist fc(90).

regula_ 13
daca{ 
   \+ bun_critic
   \+ atent_la_detalii
   \+ frica_esec 
}
atunci:
    \+ perfectionist fc(90).

regula_ 14
daca{ 
    bun_critic
    atent_la_detalii
    \+ frica_esec
}
atunci:
    perfectionist fc(80).

regula_ 15
daca{ 
    \+ bun_critic
    atent_la_detalii
    \+ frica_esec 
}
atunci:
    \+ perfectionist fc(80).

regula_ 16
daca{ 
    perspectiva:visator
    empatic:foarte
    excentric 
}
atunci:
    creativ .

regula_ 17
daca{ 
    perspectiva:realist
    empatic:deloc
   \+ excentric 
}
atunci:
    \+ creativ  .


regula_ 18
daca{ 
    perspectiva:visator
    empatic:foarte
    \+ excentric 
}
atunci:
    creativ fc(80).


regula_ 19
daca{ 
    perspectiva:realist
    empatic:putin
    excentric 
}
atunci:
    \+ creativ fc(70).


regula_ 20
daca{ 
    perspectiva:visator
    empatic:putin
    excentric 
}
atunci:
    creativ fc(80).


regula_ 21
daca{ 
    perspectiva:visator
    empatic:deloc
    excentric 
}
atunci:
    creativ fc(70).


regula_ 22
daca{ 
    perspectiva:realist
    empatic:foarte
    excentric 
}
atunci:
    \+ creativ fc(70).


regula_ 23
daca{ 
    practica_sport
    face_sala
    mers_pe_jos
}
atunci:
    fire_sportiva .

regula_ 24
daca{ 
    \+ practica_sport
    \+ face_sala
    \+ mers_pe_jos
}
atunci:
    \+ fire_sportiva .


regula_ 25
daca{ 
    practica_sport
    \+ face_sala
    mers_pe_jos
}
atunci:
    fire_sportiva  fc(90).


regula_ 26
daca{ 
    \+ practica_sport
    face_sala
    mers_pe_jos
}
atunci:
    fire_sportiva fc(70).

regula_ 27
daca{ 
    \+ practica_sport
    \+ face_sala
    mers_pe_jos
}
atunci:
    \+ fire_sportiva fc(90).

regula_ 28
daca{ 
    practica_sport
    face_sala
    \+ mers_pe_jos
}
atunci:
    fire_sportiva fc(90).

regula_ 29
daca{ 
    fire_sensibila
    perfectionist
    creativ
}
atunci:
    fire_artistica .

regula_ 30
daca{ 
    \+ fire_sensibila
    \+ perfectionist
    \+ creativ
}
atunci:
    \+ fire_artistica .


regula_ 31
daca{ 
    fire_sensibila
    \+ perfectionist
    creativ
}
atunci:
    fire_artistica fc(90).

regula_ 32
daca{ 
    \+ fire_sensibila
    \+ perfectionist
    creativ
}
atunci:
    \+ fire_artistica fc(90).


regula_ 33
daca{ 
    fire_sensibila
    perfectionist
    \+ creativ
}
atunci:
    \+ fire_artistica fc(70).


regula_ 34
daca{ 
    \+ fire_sensibila
    \+ perfectionist
    creativ
}
atunci:
    fire_artistica fc(70).

regula_ 35
daca{ 
    ii_place_cafeaua
    fire_sportiva
}
atunci:
    nivel_energetic:ridicat.

regula_ 36
daca{ 
    \+ ii_place_cafeaua
    \+ fire_sportiva
}
atunci:
    nivel_energetic:scazut.

regula_ 37
daca{ 
    ii_place_cafeaua
    \+ fire_sportiva
}
atunci:
    nivel_energetic:ridicat fc(70).


regula_ 38
daca{ 
    \+ ii_place_cafeaua
    fire_sportiva
}
atunci:
    nivel_energetic:ridicat fc(80).

regula_ 39
daca{ 
    statut_financiar:bursier
    dispus_sa_plateasca
}
atunci:
    tip_acces:bilet .

regula_ 40
daca{ 
    statut_financiar:bursier
    \+ dispus_sa_plateasca
}
atunci:
    tip_acces:gratis .

regula_ 41
daca{ 
    statut_financiar:salariat
    dispus_sa_plateasca
}
atunci:
    tip_acces:bilet .

regula_ 42
daca{ 
    statut_financiar:salariat
    \+ dispus_sa_plateasca
}
atunci:
    tip_acces:gratis .


regula_ 43
daca{ 
    statut_financiar:pensionar
    dispus_sa_plateasca
}
atunci:
    tip_acces:bilet fc(70).

regula_ 44
daca{ 
    statut_financiar:pensionar
    \+ dispus_sa_plateasca
}
atunci:
    tip_acces:gratis fc(70).

regula_ 45
daca{ 
    statut_financiar:somer
    dispus_sa_plateasca
}
atunci:
    tip_acces:gratis fc(90).

regula_ 46
daca{ 
    statut_financiar:somer
    \+ dispus_sa_plateasca
}
atunci:
    tip_acces:gratis fc(90).

regula_ 47
daca{ 
    memorie_predominanta:vizuala
    \+ fire_artistica
    nivel_energetic:scazut
    grupa_varsta:adolescent
    tip_acces:bilet
    amplasare_loc:in_spate
}
atunci:
    eveniment:film fc(70).

regula_ 48
daca{ 
    memorie_predominanta:auditiva
    fire_artistica
    nivel_energetic:scazut
    grupa_varsta:varstnic
    tip_acces:bilet
    amplasare_loc:in_spate
}
atunci:
    eveniment:opera fc(80).


regula_ 49
daca{ 
    memorie_predominanta:auditiva
    fire_artistica
    nivel_energetic:ridicat
    grupa_varsta:adolescent
    tip_acces:bilet
    amplasare_loc:in_fata
}
atunci:
    eveniment:concert fc(80).


regula_ 50
daca{ 
    memorie_predominanta:auditiva
    \+ fire_artistica
    nivel_energetic:ridicat
    grupa_varsta:adolescent
    tip_acces:bilet
    amplasare_loc:in_fata
}
atunci:
    eveniment:concert fc(80).

regula_ 51
daca{ 
    memorie_predominanta:auditiva
    \+ fire_artistica
    nivel_energetic:ridicat
    grupa_varsta:adolescent
    tip_acces:bilet
    amplasare_loc:in_fata
}
atunci:
    eveniment:opera fc(70).


regula_ 52
daca{ 
    memorie_predominanta:vizuala
    \+ fire_artistica
    nivel_energetic:scazut
    grupa_varsta:adolescent
    tip_acces:bilet
    amplasare_loc:in_spate
}
atunci:
    eveniment:circ fc(80).

regula_ 53
daca{ 
    memorie_predominanta:vizuala
    \+ fire_artistica
    nivel_energetic:ridicat
    grupa_varsta:adult
    tip_acces:bilet
    amplasare_loc:in_spate
}
atunci:
    eveniment:meci fc(70).

regula_ 54
daca{ 
    memorie_predominanta:olfactiva
    \+ fire_artistica
    nivel_energetic:scazut
    grupa_varsta:adult
    tip_acces:gratis
}
atunci:
    eveniment:streetfood_festival fc(70).

regula_ 54
daca{ 
    memorie_predominanta:olfactiva
    fire_artistica
    nivel_energetic:ridicat
    grupa_varsta:adolescent
    tip_acces:gratis
}
atunci:
    eveniment:streetfood_festival fc(80).


?: mod_lucru
cu valorile= (individual; in_echipa)
intrebare: 'Preferati sa lucrati in echipa sau individual?'.

?: mediu
cu valorile= (singur; insotit)
intrebare: 'Preferati sa petreceti timp singur sau insotit de alte persoane?'.

?: atitudine
cu valorile= (rezervata; entuziasta)
intrebare: 'Alegeti tipul de atitudine care va defineste?'.

?: usor_de_impresionat
cu valorile= (da; nu)
intrebare: 'Sunteti o persoana usor de impresionat?'.

?: bun_critic
cu valorile= (da; nu)
intrebare: 'Va considerati un critic bun?'.

?: atent_la_detalii
cu valorile= (da; nu)
intrebare: 'Sunteti o persoana atenta la detalii?'.

?: frica_esec
cu valorile= (da; nu)
intrebare: 'Va este frica de esec?'.

?: perspectiva
cu valorile= (realist; visator)
intrebare: 'Sunteti o persoana visatoare sau realista?'.

?: empatic
cu valorile= (foarte; putin; deloc)
intrebare: 'Sunteti o persoana empatica?'.

?: excentric
cu valorile= (da; nu)
intrebare: 'Sunteti o persoana excentrica?'.

?: practica_sport
cu valorile= (da; nu)
intrebare: 'Sunteti un practicant activ al vreo unui sport?'.

?: face_sala
cu valorile= (da; nu)
intrebare: 'Mergeti la sala la momentul actual?'.

?: mers_pe_jos
cu valorile= (des; rar)
intrebare: 'Cat de des alegeti sa mergeti pe jos?'.

?: ii_place_cafeaua
cu valorile= (da; nu)
intrebare: 'Sunteti un consumator inrait de cafea?'.

?: statut_financiar
cu valorile= (bursier; salariat; somer; pensionar)
intrebare: 'Alegeti statutul financiar in care va aflati in momentul actual?'.

?: dispus_sa_plateasca
cu valorile= (da; nu)
intrebare: 'Sunteti dispus sa platiti pentru un anumit eveniment?'.

?: grupa_varsta
cu valorile= (adolescent; adult; varstnic)
intrebare: 'In ce grupa de varsta va incadrati?'.

?: amplasare_loc
cu valorile= (in_fata; in_spate; in_centru)
intrebare: 'Unde preferati sa fie locul dumneavoastra?'.

?: memorie_predominanta
cu valorile= (auditiva; vizuala; olfactiva)
intrebare: 'Alegeti tipul de memorie predominanta?'.