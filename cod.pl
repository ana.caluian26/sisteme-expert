/*
cod preluat din cartea(bibliografie[1]):
BALCAN Maria Florina, HRISTEA Florentina, 
Aspecte ale Cautarii si Reprezentarii Cunostintelor in Inteligenta Artificiala,
Editura Universitatii din Bucuresti, 2004, 
pg 216
*/

close_all:-current_stream(_,_,S),close(S),fail;true.

curata_bc:-current_predicate(P), abolish(P,[force(true)]), fail;true.


:-use_module(library(lists)).
:-use_module(library(sockets)).
:-use_module(library(file_systems)).
:-use_module(library(system)).

:-op(900,fy,not).

:-dynamic fapt/3.

:-dynamic interogat/1.

:-dynamic scop/1.

:-dynamic interogabil/3.

:-dynamic regula/3.

:-dynamic solutie/4.

:-dynamic count/1.

tab(N):-N>0,write(' '),N1 is N-1, tab(N1).
tab(0).


not(P):-P,!,fail.

not(_).


scrie_lista([]):-nl.

scrie_lista([H|T]) :-
	write(H), tab(1),
	scrie_lista(T).

             
afiseaza_fapte :-
	write('Fapte existente in baza de cunostinte:'),
	nl,nl, write(' (Atribut,valoare) '), nl,nl,
	listeaza_fapte,nl.


listeaza_fapte:- 
	asserta(count(1)),
	fapt(av(Atr,Val),FC,_), 
	retract(count(N)),
	write(N), 
	N1 is N +1,
	asserta(count(N1)),
	write('.'),
	write(Atr),write('='),
	write(Val),
	write(','), write(' FC '),
	FC1 is integer(FC),write(FC1),
	nl,fail.

listeaza_fapte.

lista_float_int([],[]).

lista_float_int([Regula|Reguli],[Regula1|Reguli1]):-
	(Regula \== utiliz,
	Regula1 is integer(Regula);
	Regula ==utiliz, Regula1=Regula),
	lista_float_int(Reguli,Reguli1).


pornire :-
	retractall(interogat(_)),
	retractall(fapt(_,_,_)),
	repeat,
	write('Introduceti una din urmatoarele optiuni: '),
	nl,nl,
	write(' (Incarca Consulta Reinitiaza  Afisare_fapte  Cum   Iesire) '),
	nl,nl,write('|: '),citeste_linie([H|T]),
	executa([H|T]), H == iesire.

executa([incarca]) :- 
	incarca(1),incarca_descriere,!,nl,
	write('Fisierul dorit a fost incarcat'),nl.

executa([consulta]) :- 
	deletedirector('demonstratii'),
	scopuri_princ,!.


executa([reinitiaza]) :- 
	retractall(interogat(_)),
	retractall(fapt(_,_,_)),!.


executa([afisare_fapte]) :-
	afiseaza_fapte,!.


executa([cum|L]) :- cum(L),!.

executa([iesire]):-!.

executa([_|_]) :-
	write('Comanda incorecta! '),nl.


scopuri_princ :-
	scop(Atr),
	determina(Atr),
	(fapt(av(Atr,Val),FC,_)->
		ord_sol,
	afiseaza_scop(Atr);
	write('Sistemul nu are solutii'),nl
	),
	fail.

scopuri_princ.


determina(Atr) :-
realizare_scop(av(Atr,_),_,[scop(Atr)]),!.

determina(_).


afiseaza_scop(Atr) :-
	fapt(av(Atr,Val),FC,_),
		solutie(Val,Img,Prop,Desc),
		FC >= 20,
		scrie_scop(av(Atr,Val),FC,Desc,Img,Prop),nl,
		demonstratie_fisier(av(Atr,Val)),
		fail.


	ord_sol :- setof(fapt(FC,Atr,Val,Ist),retract(fapt(av(Atr,Val),FC,Ist)),ListaOrd),insertOrd(ListaOrd).
	insertOrd([fapt(FC,Atr,Val,Ist)|T]):- asserta(fapt(av(Atr,Val),FC,Ist)),insertOrd(T).
	insertOrd([]).


afiseaza_scop(_):-nl,meniu_secundar,!.


meniu_secundar:-
					repeat,
					nl,
					write('Alegeti una din urmatoarele optiuni: '),
					nl,nl,
					write(' (Doar_desc | Tabel_desc | Meniu_inapoi ) '),
					nl,nl,write('|: '),citeste_linie([H]),nl,
					executa_secundar([H]), H == meniu_inapoi.

executa_secundar([doar_desc]) :- scop(Atr),
								afis_ddescriere(Atr),
								!.


afis_ddescriere(Atr) :-	fapt(av(Atr,Val),_,_),
						nl,
						solutie(Val,Img,_,Desc),
						write('Descriere:'),
						write(Desc),
						nl,
						write('Imagine:'),
						write(Img),nl,fail.

afis_ddescriere(_) :- meniu_secundar,!.					
				


executa_secundar([tabel_desc]):-scop(Atr),
								afis_tabel(Atr),
								!.

afis_tabel(Atr) :-	format('~`_t Descriere ~`_t~61|~n', []),
					fapt(av(Atr,Val),_,_),
					nl,
					solutie(Val,Img,_,Desc),
					clean_string_delimiters(Desc,DescNou),
					 format('`|~t~a~t~20|~t~a~t~20+~t`|~61|~n',[Val,DescNou]),
					 format('~`_t~61|~n', []),
					fail.

afis_tabel(_) :- meniu_secundar,!.		

clean_string_delimiters(String, Result) :-
				atom_length(String, Str_length),
				Final_length is Str_length - 2,
				sub_atom(String, 1, Final_length, 1, Result).

executa_secundar([meniu_inapoi]):-!.

executa_secundar([_]) :-write('Comanda incorecta'),nl. 

%scrie_scop(av(Atr,Val),FC,Desc,Img,Prop) :-

scrie_scop(av(Atr,Val),FC,Desc,Img,Prop):-
	transformare(av(Atr,Val), X),
	scrie_lista(X),tab(2),
	write(' '),
	write('factorul de certitudine este '),
	FC1 is integer(FC),write(FC1),
	nl,
	write('Imagine:'),write(Img),nl,
	write('Descriere:'),nl,write(Desc),nl,
	write('Prop:'),scrie_prop(Prop),nl,
	write('-------------------').
%	nl,
%	write('Imagine:'),write(Img),nl,
%	write('Descriere:'),nl,write(Desc),nl,
%	write('Prop:'),scrie_prop(Prop),nl,
%	write('-------------------').

scrie_prop([av(A1,V1)|Prop]) :- nl,
									write(A1),
									write('='),
									write(V1),
									scrie_prop(Prop).

scrie_prop([_]).

realizare_scop(not Scop,Not_FC,Istorie) :-
	realizare_scop(Scop,FC,Istorie),
	Not_FC is - FC, !.

realizare_scop(Scop,FC,_) :-
	fapt(Scop,FC,_), !.

realizare_scop(Scop,FC,Istorie) :-
	pot_interoga(Scop,Istorie),
	!,realizare_scop(Scop,FC,Istorie).

realizare_scop(Scop,FC_curent,Istorie) :-
	fg(Scop,FC_curent,Istorie).

        
fg(Scop,FC_curent,Istorie) :-
	regula(N, premise(Lista), concluzie(Scop,FC)),
	demonstreaza(N,Lista,FC_premise,Istorie),
	ajusteaza(FC,FC_premise,FC_nou),
	actualizeaza(Scop,FC_nou,FC_curent,N),
	FC_curent == 100,!.

fg(Scop,FC,_) :- fapt(Scop,FC,_).


pot_interoga(av(Atr,_),Istorie) :-
	not interogat(av(Atr,_)),
	interogabil(Atr,Optiuni,Mesaj),
	interogheaza(Atr,Mesaj,Optiuni,Istorie),nl,
	asserta( interogat(av(Atr,_)) ).




demonstratie_fisier(Scop) :-
						isdirector('demonstratii'),
						generate_slug(Scop,Nume),
						atom_concat('demonstratii/',Nume,Path),
						telling(Current_input),
						tell(Path),
						transformare(Scop,L),
						executa([cum|L]),
						told,
						tell(Current_input),!.



isdirector(Nume):-
				(
					directory_exists(Nume)->
					true
					;
					make_directory(Nume)
				).


deletedirector(Nume):-
					(
						directory_exists(Nume)->
						delete_directory(Nume,[if_nonempty(delete)])
						;
						true
					).

generate_slug(av(Atr,Solutie),Nume):-
									fapt(av(Atr,Solutie),FC,_),
									file_name(Solutie,FC,Nume).

file_name(Solutie,FC,FileName) :- convert_nr_to_atom(FC,FcNr),
									atom_concat('demonstratie_(solutie#',Solutie,A),
									atom_concat(A,'#',B),
									atom_concat(B,')',C),
									atom_concat(C,'[',D),
									atom_concat(D,'factcert#',E),
									atom_concat(E,FcNr,F),
									atom_concat(F,'#].txt',FileName).

convert_nr_to_atom(Nr,Atom):- number_chars(Nr,Lchr),
								atom_chars(Atom,Lchr).


cum([]) :- write('Scop? '),nl,
	write('|:'),citeste_linie(Linie),nl,
	transformare(Scop,Linie), cum(Scop).

cum(L) :- 
	transformare(Scop,L),nl, cum(Scop).

cum(not Scop) :- 
	fapt(Scop,FC,Reguli),
	lista_float_int(Reguli,Reguli1),
	FC < -20,transformare(not Scop,PG),
	append(PG,[a,fost,derivat,cu, ajutorul, 'regulilor: '|Reguli1],LL),
	scrie_lista(LL),nl,afis_reguli(Reguli),fail.

cum(Scop) :-
	fapt(Scop,FC,Reguli),
	lista_float_int(Reguli,Reguli1),
	FC > 20,transformare(Scop,PG),
	append(PG,[a,fost,derivat,cu, ajutorul, 'regulilor: '|Reguli1],LL),
	scrie_lista(LL),nl,afis_reguli(Reguli),
	fail.

cum(_).





afis_reguli([]).

afis_reguli([N|X]) :-
	afis_regula(N),
	premisele(N),
	afis_reguli(X).

afis_regula(N) :-
	regula(N, premise(Lista_premise),
	concluzie(Scop,FC)),NN is integer(N),
	scrie_lista(['regula_ ',NN]),
	scrie_lista(['  daca{']),
	scrie_lista_premise(Lista_premise),
	scrie_lista([' }atunci:']),
	transformaredem(Scop,Scop_tr),
	append(['   '],Scop_tr,L1),
	FC1 is integer(FC),append(L1,[FC1],LL),
	scrie_lista(LL),nl.


scrie_lista_premise([]).

scrie_lista_premise([H|T]) :-
	transformaredem(H,H_tr),
	tab(5),scrie_lista(H_tr),
	scrie_lista_premise(T).


transformare(av(A,da),[A]) :- !.

%% transformare(not av(A,da), [not,A]) :- !.

transformare(av(A,nu),[not,A]) :- !.

transformare(av(A,V),[A,este,V]).


transformaredem(av(A,da),[A]) :- !.

transformaredem(not av(Atr,da), [(\),'+',Atr]).

transformaredem(av(A,V),[A,':',V]).



premisele(N) :-
	regula(N, premise(Lista_premise), _),
	!, cum_premise(Lista_premise).

        
cum_premise([]).

cum_premise([Scop|X]) :-
	cum(Scop),
	cum_premise(X).

        
interogheaza(Atr,Mesaj,[da,nu,nu_stiu,nu_conteaza],Istorie) :-
	!,write(Mesaj),nl,write('( da nu nu_stiu nu_conteaza)'),nl,
	de_la_utiliz(X,Istorie,[da,nu, nu_stiu, nu_conteaza]),
	(

	X \== 'nu_conteaza'->
	det_val_fc(X,Val,FC),
	asserta( fapt(av(Atr,Val),FC,[utiliz]) )
	;
	asserta( fapt(av(Atr,da),100,[utiliz]) ),
	asserta( fapt(av(Atr,da),-100,[utiliz]) )
	).

interogheaza(Atr,Mesaj,Optiuni,Istorie) :-
	write(Mesaj),nl,
	citeste_opt(X,Optiuni,Istorie),
	(X \== [nu_conteaza] ->
	assert_fapt(Atr,X);
	append(ListaFaraNustiuNuconteaza,[nu_stiu,nu_conteaza],Optiuni),
	assert_fapt(Atr,X,ListaFaraNustiuNuconteaza)
	).


citeste_opt(X,Optiuni,Istorie) :-
	append(['('],Optiuni,Opt1),
	append(Opt1,[')'],Opt),
	scrie_lista(Opt),
	de_la_utiliz(X,Istorie,Optiuni).


de_la_utiliz(X,Istorie,Lista_opt) :-
	repeat,write(': '),citeste_linie(X),
	proceseaza_raspuns(X,Istorie,Lista_opt).


proceseaza_raspuns([de_ce],Istorie,_) :- nl,afis_istorie(Istorie),!,fail.


proceseaza_raspuns([X],_,Lista_opt):-
	member(X,Lista_opt).

proceseaza_raspuns([X,fc,FC],_,Lista_opt):-
	member(X,Lista_opt),float(FC).


assert_fapt(Atr,[Val,fc,FC]) :-
!,asserta( fapt(av(Atr,Val),FC,[utiliz]) ).

assert_fapt(Atr,[Val]) :-
asserta( fapt(av(Atr,Val),100,[utiliz])).


assert_fapt(Atr,_,[H|T]) :-
!,asserta( fapt(av(Atr,H),100,[utiliz]) ),assert_fapt(Atr,_,T).

assert_fapt(Atr,_,[]).


det_val_fc([nu],da,-100).

det_val_fc([nu,FC],da,NFC) :- NFC is -FC.

det_val_fc([nu,fc,FC],da,NFC) :- NFC is -FC.

det_val_fc([Val,FC],Val,FC).

det_val_fc([Val,fc,FC],Val,FC).

det_val_fc([Val],Val,100).

        
afis_istorie([]) :- nl.

afis_istorie([scop(X)|T]) :-
	scrie_lista([scop,X]),!,
	afis_istorie(T).

afis_istorie([N|T]) :-
	afis_regula(N),!,afis_istorie(T).


demonstreaza(N,ListaPremise,Val_finala,Istorie) :-
	dem(ListaPremise,100,Val_finala,[N|Istorie]),!.


dem([],Val_finala,Val_finala,_).

dem([H|T],Val_actuala,Val_finala,Istorie) :-
	realizare_scop(H,FC,Istorie),
	Val_interm is min(Val_actuala,FC),
	Val_interm >= 20,
	dem(T,Val_interm,Val_finala,Istorie).

 
actualizeaza(Scop,FC_nou,FC,RegulaN) :-
	fapt(Scop,FC_vechi,_),
	combina(FC_nou,FC_vechi,FC),
	retract( fapt(Scop,FC_vechi,Reguli_vechi) ),
	asserta( fapt(Scop,FC,[RegulaN | Reguli_vechi]) ),!.

actualizeaza(Scop,FC,FC,RegulaN) :-
	asserta( fapt(Scop,FC,[RegulaN]) ).


ajusteaza(FC1,FC2,FC) :-
	X is FC1 * FC2 / 100,
	FC is round(X).

combina(FC1,FC2,FC) :-
	FC1 >= 0,FC2 >= 0,
	X is FC2*(100 - FC1)/100 + FC1,
	FC is round(X).

combina(FC1,FC2,FC) :-
	FC1 < 0,FC2 < 0,
	X is - ( -FC1 -FC2 * (100 + FC1)/100),
	FC is round(X).

combina(FC1,FC2,FC) :-
	(FC1 < 0; FC2 < 0),
	(FC1 > 0; FC2 > 0),
	FCM1 is abs(FC1),FCM2 is abs(FC2),
	MFC is min(FCM1,FCM2),
	X is 100 * (FC1 + FC2) / (100 - MFC),
	FC is round(X).


incarca(1) :-
	%write('Introduceti numele fisierului care doriti sa fie incarcat: '),nl, write('|:'),
	F ='merged.txt',
	file_exists(F),!,incarca(F).

incarca_descriere:-
	G ='descriere.txt',
	file_exists(G),!,incarca_descriere(G).

incarca:-write('Nume incorect de fisier! '),nl,fail.

incarca_descriere:-write('Nume incorect de fisier descriere! '),nl,fail.

incarca(F) :-
	retractall(interogat(_)),retractall(fapt(_,_,_)),
	retractall(scop(_)),retractall(interogabil(_,_,_)),
	retractall(regula(_,_,_)),
	see(F),incarca_reguli,seen,!.

/*incarca fisierul de descrieri*/

incarca_descriere(G) :-
	retractall(solutie(_,_,_,_)),
	see(G),incarca_reguli,seen,!.


incarca_reguli :-
	repeat,citeste_propozitie(L),nl,
	proceseaza(L),L == [end_of_file].


proceseaza([end_of_file]):-!.

proceseaza(L) :-
	trad(R,L,[]),
	(R = interogabil(Atr,M,P)->append(M,[nu_stiu,nu_conteaza],MFinal),
	assertz(interogabil(Atr,MFinal,P));assertz(R)), !.

trad(scop(X)) --> [scop,':',X].


trad(interogabil(Atr,M,P)) --> 
 	['?',':',Atr],lista_optiuni(M),afiseaza(Atr,P).
	

trad(regula(N,premise(Daca),concluzie(Atunci,F))) --> identificator(N),daca(Daca),atunci(Atunci,F).

trad(solutie(Nume,Imagine,Prop,Descriere)) -->
			numeSolutie(Nume),
			caleImagine(Imagine),
			listaProprietati(Prop),
			continutDescriere(Descriere),
			['-'].

numeSolutie(N) --> [solutie,':','(',N,')'].

continutDescriere(D) --> [descriere,':','(',D,')'].

caleImagine(I) --> [imagine,':','(',I,')'].

listaProprietati(Prop) --> [proprietati,'=','{'],lista_proprietati(Prop).

lista_proprietati([Prop]) -->propoz(Prop),['}'].

lista_proprietati([H|T]) -->propoz(H),lista_proprietati(T).

propoz(av(Atr,Val)) --> ['[',Atr,'=',Val,']'].

trad('Eroare la parsare'-L,L,_).


lista_optiuni(M) --> [cu,valorile,'=','('],lista_de_optiuni(M).

lista_de_optiuni([Element]) -->  [Element,')'].

lista_de_optiuni([Element|T]) --> [Element,';'],lista_de_optiuni(T).


afiseaza(_,P) -->  [intrebare,':',P].

%afiseaza(P,P) -->  [].

identificator(N) --> [regula_,N].


daca(Daca) --> [daca,'{'],lista_premise(Daca).


lista_premise([Daca]) --> propoz_premisa(Daca),['}',atunci,':'].


lista_premise([Prima|Celalalte]) --> propoz_premisa(Prima),lista_premise(Celalalte).


atunci(Atunci,FC) --> propoz_concluzie(Atunci),[fc,'(',FC,')'].

atunci(Atunci,100) --> propoz_concluzie(Atunci).


propoz_premisa(not av(Atr,da)) --> [(\),'+',Atr].

propoz_premisa(av(Atr,Val)) --> [Atr,':',Val].

propoz_premisa(av(Atr,da)) --> [Atr].



propoz_concluzie(not av(Atr,da)) --> [(\),'+',Atr].

propoz_concluzie(av(Atr,Val)) --> [Atr,':',Val].

propoz_concluzie(av(Atr,da)) --> [Atr].

citeste_linie([Cuv|Lista_cuv]) :-
	get_code(Car),
	citeste_cuvant(Car, Cuv, Car1), 
	rest_cuvinte_linie(Car1, Lista_cuv).
      
% -1 este codul ASCII pt EOF

rest_cuvinte_linie(-1, []):-!.
    
rest_cuvinte_linie(Car,[]) :-(Car==13;Car==10), !.

rest_cuvinte_linie(Car,[Cuv1|Lista_cuv]) :-
	citeste_cuvant(Car,Cuv1,Car1),      
	rest_cuvinte_linie(Car1,Lista_cuv).


citeste_propozitie([Cuv|Lista_cuv]) :-
	get_code(Car),citeste_cuvant(Car, Cuv, Car1), 
	rest_cuvinte_propozitie(Car1, Lista_cuv).
 
     
rest_cuvinte_propozitie(-1, []):-!.
    
rest_cuvinte_propozitie(Car,[]) :-Car==46,!.

rest_cuvinte_propozitie(Car,[]) :-Car==45,sfarsit_descriere(8),!.

sfarsit_descriere(0) :- !.

sfarsit_descriere(N) :- get_code(_),
						N1 is N - 1,
						sfarsit_descriere(N1).

rest_cuvinte_propozitie(Car,[Cuv1|Lista_cuv]) :-
	citeste_cuvant(Car,Cuv1,Car1),      
	rest_cuvinte_propozitie(Car1,Lista_cuv).


citeste_cuvant(-1,end_of_file,-1):-!.

citeste_cuvant(Caracter,Cuvant,Caracter1) :-   
	caracter_cuvant(Caracter),!, 
	name(Cuvant, [Caracter]),get_code(Caracter1).

citeste_cuvant(Caracter, Numar, Caracter1) :-
	caracter_numar(Caracter),!,
	citeste_tot_numarul(Caracter, Numar, Caracter1).
 

citeste_tot_numarul(Caracter,Numar,Caracter1):-
	determina_lista(Lista1,Caracter1),
	append([Caracter],Lista1,Lista),
	transforma_lista_numar(Lista,Numar).


determina_lista(Lista,Caracter1):-
	get_code(Caracter), 
	(caracter_numar(Caracter),
	determina_lista(Lista1,Caracter1),
	append([Caracter],Lista1,Lista); 
	\+(caracter_numar(Caracter)),
	Lista=[],Caracter1=Caracter).
 

transforma_lista_numar([],0).

transforma_lista_numar([H|T],N):-
	transforma_lista_numar(T,NN), 
	lungime(T,L), Aux is exp(10,L),
	HH is H-48,N is HH*Aux+NN.


lungime([],0).

lungime([_|T],L):-
	lungime(T,L1),
	L is L1+1.


% 39 este codul ASCII pt '

citeste_cuvant(Caracter,Cuvant,Caracter1) :-
	Caracter==39,!,
	pana_la_urmatorul_apostrof(Lista_caractere),
	L=[Caracter|Lista_caractere],
	name(Cuvant, L),get_code(Caracter1).
        

pana_la_urmatorul_apostrof(Lista_caractere):-
	get_code(Caracter),
	(Caracter == 39,Lista_caractere=[Caracter];
	Caracter\==39,
	pana_la_urmatorul_apostrof(Lista_caractere1),
	Lista_caractere=[Caracter|Lista_caractere1]).


citeste_cuvant(Caracter,Cuvant,Caracter1) :-          
	caractere_in_interiorul_unui_cuvant(Caracter),!,              
	((Caracter>64,Caracter<91),!,
	Caracter_modificat is Caracter+32;
	Caracter_modificat is Caracter),                              
	citeste_intreg_cuvantul(Caractere,Caracter1),
	name(Cuvant,[Caracter_modificat|Caractere]).
        

citeste_intreg_cuvantul(Lista_Caractere,Caracter1) :-
	get_code(Caracter),
	(caractere_in_interiorul_unui_cuvant(Caracter),
	((Caracter>64,Caracter<91),!, 
	Caracter_modificat is Caracter+32;
	Caracter_modificat is Caracter),
	citeste_intreg_cuvantul(Lista_Caractere1, Caracter1),
	Lista_Caractere=[Caracter_modificat|Lista_Caractere1]; \+(caractere_in_interiorul_unui_cuvant(Caracter)),
	Lista_Caractere=[], Caracter1=Caracter).


citeste_cuvant(_,Cuvant,Caracter1) :-                
	get_code(Caracter),       
	citeste_cuvant(Caracter,Cuvant,Caracter1).
 

caracter_cuvant(C):-member(C,[44,59,58,63,33,46,41,40,43,123,125,92,61,45,91,93]).


% am specificat codurile ASCII pentru , ; : ? ! . ) ( +  { } \ =

caractere_in_interiorul_unui_cuvant(C):-
	C>64,C<91;C>47,C<58;
	C==45;C==95;C>96,C<123.

caracter_numar(C):-C<58,C>=48.

